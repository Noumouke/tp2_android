package com.example.tp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tp2_app.R;

public class AjouterWine extends AppCompatActivity {

    EditText winename;
    EditText region;
    EditText localisation;
    EditText climat;
    EditText Superface_plat;
    Button sauve;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajouter_wine);

        final WineDbHelper winedb=new WineDbHelper(getApplicationContext());

        winename=(EditText) findViewById(R.id.wineName);
        region=(EditText) findViewById(R.id.editWineRegion);
        localisation=(EditText) findViewById(R.id.editLoc);
        climat=(EditText) findViewById(R.id.editClimate);
        Superface_plat=(EditText) findViewById(R.id.editPlantedArea);
        sauve=(Button) findViewById(R.id.button);


        sauve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String winenam=String.valueOf(winename.getText());
                String regio=String.valueOf(region.getText());
                String localisa=String.valueOf(localisation.getText());
                String clima=String.valueOf(climat.getText());
                String Superface=String.valueOf(Superface_plat.getText());

                if(winenam.matches("")){
                    AlertDialog.Builder builder= new AlertDialog.Builder(AjouterWine.this);
                    builder.setTitle("Impossible de de saugarder");
                    builder.setMessage("le nom de vin ne doit pas etre vide ");
                    builder.setCancelable(true);
                    builder.show();
                    return;


                }

                else {
                    Wine wine = new Wine(winenam, regio, localisa, clima, Superface);
                    if (!winedb.addWine(wine)) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                        builder.setTitle("Ajout impossible");
                        builder.setMessage("le vin exite deja dans la base de donnée ");
                        builder.setCancelable(true);
                        builder.show();
                       return;

                    }
                   else {

                        Toast.makeText(AjouterWine.this, "SAUVGARDAGE DES DONNEES", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }
}
