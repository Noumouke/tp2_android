package com.example.tp;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import com.example.tp2_app.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.ContextMenu;
import android.view.View;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {

    ListView list;
   // private MenuItem item;
    WineDbHelper dbs;
    SimpleCursorAdapter Adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        list= (ListView) findViewById(R.id.listeView);
        dbs= new WineDbHelper(getApplicationContext());
       dbs.populate();
        final Cursor cursor=dbs.fetchAllWines();

        Adapter=new SimpleCursorAdapter(MainActivity.this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[]{WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION},
                new  int[] {android.R.id.text1, android.R.id.text2},0

                );
        list.setAdapter(Adapter);

        final Intent intent=new Intent(this, WineActivity.class);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor =(Cursor) list.getItemAtPosition(position);

                Wine wine=dbs.cursorToWine(cursor);
                intent.putExtra("wine", wine);
                startActivity(intent);
            }
        });



        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ajouter = new Intent(getApplicationContext(), AjouterWine.class);
                startActivity(ajouter);
            }
        });



        /*fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();


            }
        });*/

      list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
          @Override
          public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                         int pos, long id) {


              registerForContextMenu(list);
              return false;
          }
      });
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        menu.add(0, v.getId(), 0, "Supprimer");
    }

   @Override
    public boolean onContextItemSelected(MenuItem item){
        if(item.getTitle()=="Supprimer"){
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            Cursor cur = (Cursor)list.getItemAtPosition(info.position);

            Wine wine=dbs.cursorToWine(cur);
            dbs.deleteWine(cur);
            Cursor cursor = dbs.fetchAllWines();
            Adapter.changeCursor(cursor);
            Adapter.notifyDataSetChanged();

        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
   @Override
    protected void onResume() {
        super.onResume();
        System.out.println("MainActivity :: onResume()");
        Cursor cursor = dbs.fetchAllWines();
        Adapter.changeCursor(cursor);
        Adapter.notifyDataSetChanged();

    }
}
